-- {"regionName", xCenter, yCenter, shape and size, tier, {"spawnGroup1", ...}, maxSpawnLimit}
-- Shape and size is a table with the following format depending on the shape of the area:
--   - Circle: {1, radius}
--   - Rectangle: {2, width, height}
--   - Ring: {3, inner radius, outer radius}
-- Tier is a bit mask with the following possible values where each hexadecimal position is one possible configuration.
-- That means that it is not possible to have both a spawn area and a no spawn area in the same region, but
-- a spawn area that is also a no build zone is possible.

require("scripts.managers.spawn_manager.regions")

ordmantell_regions = {
	{"world_spawner",0,0,{1,-1},SPAWNAREA + WORLDSPAWNAREA + NOBUILDZONEAREA,{"ord_mantell_world"},2048},
	{"junkyard",-3000,-1000,{1,500},SPAWNAREA + NOWORLDSPAWNAREA + NOBUILDZONEAREA,{"ord_mantell_junkyard"},256},
	{"city",-340,-3500,{1,500},NOSPAWNAREA + NOBUILDZONEAREA},
	{"smugglers_outpost",3900,3700,{1,500},NOSPAWNAREA + NOBUILDZONEAREA},
	{"treilans_lab",6000,6700,{1,250},NOSPAWNAREA + NOBUILDZONEAREA},
	{"destroyed_city",-2230,2637,{1,300},NOSPAWNAREA + NOBUILDZONEAREA},
}
